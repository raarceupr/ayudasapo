#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QPropertyAnimation>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    // We will use a label to contain the coqui image
    QPixmap f(":/images/coqui.jpg");
    lab = new QLabel(this);
    lab->setPixmap(f.scaled(30,30,Qt::KeepAspectRatio));
    lab->resize(30,30);
    // initially it will be hiden. It will be made visible during the animation.
    lab->hide();

}

MainWindow::~MainWindow()
{
    delete lab;
    delete ui;
}

void MainWindow::on_pushButton_clicked()
{
    QPainterPath starPath;
    int wSapo = 100;
    int wPond = 200;
    int xTicks = 30;
    int hPond = 200;
    float xFactor = (float)wPond / xTicks;
    float yFactor = xFactor;


    int scaledX, scaledY;

    QPropertyAnimation *animation = new QPropertyAnimation(lab,"geometry",this);

    animation->setDuration(1500);
    animation->setEasingCurve(QEasingCurve::Linear);
    animation->setLoopCount(1);

    // sets the path of the animation to a parabola

    for( double x = 0 ; x < 100; x = x + 1) {
        float y = -.1*x*x + 3.4*x;

        scaledX = x * xFactor;
        scaledY = hPond - (y * yFactor + 20);

        animation->setKeyValueAt(x/100.0,QRect(QPoint(scaledX,scaledY),QSize(lab->width(),lab->height())));
    }
    lab->show();
    animation->start();
    QObject::connect(animation, &QPropertyAnimation::finished,this, &MainWindow::endJump);

}

void MainWindow::endJump() {
    lab->hide();
}
